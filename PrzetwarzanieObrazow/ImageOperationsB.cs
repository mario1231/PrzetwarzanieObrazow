/*
 *  Operacje z punktów 5-9
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace PrzetwarzanieObrazow
{
    public static partial class ImageOperations
    {
        /*
         * Funkcja konwertująca obraz 8-bitowy na binarny
        */
        public static MatrixImage ToBinary(MatrixImage image)
        {
            for (int i = 0; i < image.Channels; ++i)
                for (int j = 0; j < image.Height; ++j)
                    for (int k = 0; k < image.Width; ++k)
                        image.ImageMatrix[i][j, k] = (byte)(image.ImageMatrix[i][j, k] < 128 ? 0 : 255);

            return image;
        }

        /*
         * Funkcja sprawdza czy podany piksel jest krawędzią obrazu
        */
        public static bool IsOnEdge(MatrixImage image, int x, int y)
        {
            if (image.ImageMatrix[0][x, y] != 0)
                return false;

            if (x > 0 && image.ImageMatrix[0][x - 1, y] != 0 ||
                    x > 0 && y < image.Width - 2 && image.ImageMatrix[0][x - 1, y + 1] != 0 ||
                    y < image.Width - 2 && image.ImageMatrix[0][x, y + 1] != 0 ||
                    x < image.Height - 2 && y < image.Width - 2 && image.ImageMatrix[0][x + 1, y + 1] != 0 ||
                    x < image.Height - 2 && image.ImageMatrix[0][x + 1, y] != 0 ||
                    x < image.Height - 2 && y > 0 && image.ImageMatrix[0][x + 1, y - 1] != 0 ||
                    y > 0 && image.ImageMatrix[0][x, y - 1] != 0 ||
                    x > 0 && y > 0 && image.ImageMatrix[0][x - 1, y - 1] != 0
                )
                return true;

            return false;
        }

        /*
         *  Funkcja sprawdza czy obraz B w podanych współrzędnych w całości zawiera się w obrazie A
         */
        public static bool IsBInA(MatrixImage imageA, MatrixImage imageB, int x, int y)
        {
            for (int j = 0; j < imageB.Height; ++j)
                for (int k = 0; k < imageB.Width; ++k)
                    if (imageB.ImageMatrix[0][j, k] != 0 && imageA.ImageMatrix[0][x + j, y + k] == 0)
                        return false;
            
            return true;
        }

        /*
         *  Funkcja negująca obraz
         */
        public static MatrixImage Negation(MatrixImage image)
        {
            MatrixImage resultImage = new MatrixImage(image);

            for (int j = 0; j < image.Height; ++j)
                for (int k = 0; k < image.Width; ++k)
                    resultImage.ImageMatrix[0][j, k] = (byte)(255 - image.ImageMatrix[0][j, k]);

            return resultImage;
        }

        /*
         * Funkcja 
         */
        public static double[][] CalculateHistogram(MatrixImage image)
        {
            double[][] histogram = new double[image.Channels][];
            for (int i = 0; i < image.Channels; ++i)
            {
                histogram[i] = new double[256];

                for (int j = 0; j < 256; ++j)
                    histogram[i][j] = 0;
            }

            for (int i = 0; i < image.Channels; ++i)
                for (int j = 0; j < image.Height; ++j)
                    for (int k = 0; k < image.Width; ++k)
                        histogram[i][image.ImageMatrix[i][j, k]]++;

            for (int i = 0; i < image.Channels; ++i)
                for (int j = 0; j < 256; ++j)
                    histogram[i][j] = Math.Round(100.0 * histogram[i][j] / (double)(image.Width * image.Height), 2);

            return histogram;
        }

        /*
         * Funkcja przemieszcza histogram
         */
        public static MatrixImage MoveHistogram(MatrixImage image, int constant)
        {
            return Addition(image, constant);
        }

        /*
         * Funkcja pionowo skaluje histogram
         */
        public static MatrixImage VerticallyScaleHistogram(MatrixImage image, double constant)
        {
            return Multiplication(image, constant);
        }

        /*
         * Funkcja poziomo skaluje histogram
         */
        public static MatrixImage HorizontallyScaleHistogram(MatrixImage image, int constant)
        {
            MatrixImage resultImage = MoveHistogram(image, -constant);

            byte min = 255;
            byte max = 0;

            for (int i = 0; i < image.Channels; ++i)
            {
                for (int j = 0; j < image.Height; ++j)
                    for (int k = 0; k < image.Width; ++k)
                    {
                        if (min > resultImage.ImageMatrix[i][j, k])
                            min = resultImage.ImageMatrix[i][j, k];

                        if (max < resultImage.ImageMatrix[i][j, k])
                            max = resultImage.ImageMatrix[i][j, k];
                    }
            }

            return VerticallyScaleHistogram(resultImage, 1.0 / ((max - min) / 255.0));
        }

        /*
         * Funkcja wykonująca progowanie lokalne
         */
        public static MatrixImage LocalThresholding(MatrixImage image, int window)
        {
            if (window <= 1)
                window = 3;
            if (window % 2 == 0)
                ++window;

            int half = (int)Math.Floor(window / 2.0);

            MatrixImage resultImage = new MatrixImage(image);

            for (int i = 0; i < image.Channels; ++i)
                for (int j = 0; j < image.Height; ++j)
                    for (int k = 0; k < image.Width; ++k)
                    {
                        int mean = 0;
                        int count = 0;

                        for (int a = Math.Max(0, j - half); a < Math.Min(image.Height, j + half); ++a)
                            for (int b = Math.Max(0, k - half); b < Math.Min(image.Width, k + half); ++b, ++count)
                                mean += image.ImageMatrix[i][a, b];

                        mean /= count;

                        resultImage.ImageMatrix[i][j, k] = (byte)(resultImage.ImageMatrix[i][j, k] < mean ? 0 : 255);
                    }

            return resultImage;
        }

        /*
         * Funkcja wykonująca progowanie globalne
         */
        public static MatrixImage GlobalThresholding(MatrixImage image, int constant)
        {
            MatrixImage resultImage = new MatrixImage(image);

            for (int i = 0; i < image.Channels; ++i)
                for (int j = 0; j < image.Height; ++j)
                    for (int k = 0; k < image.Width; ++k)
                        if (resultImage.ImageMatrix[i][j, k] <= constant)
                            resultImage.ImageMatrix[i][j, k] = 0;
                        else
                            resultImage.ImageMatrix[i][j, k] = 255;

            return resultImage;
        }

        /*
         * Funkcja wykonująca progowanie wieloprogowe
         */
        public static MatrixImage MultiThresholding(MatrixImage image, int count)
        {
            if (count > 255)
                count = 255;

            MatrixImage resultImage = new MatrixImage(image);
            int[] intervals = new int[count+2];

            intervals[0] = 0;

            for (int i = 1; i < count + 1; ++i)
                intervals[i] = (int)(i / (double)(count+1) * 255.0);

            intervals[count+1] = 256;
            
            for (int i = 0; i < image.Channels; ++i)
                for (int j = 0; j < image.Height; ++j)
                    for (int k = 0; k < image.Width; ++k)
                    {
                        if (image.ImageMatrix[i][j, k] < intervals[1])
                            resultImage.ImageMatrix[i][j, k] = 0;
                        else if (image.ImageMatrix[i][j, k] >= intervals[count])
                            resultImage.ImageMatrix[i][j, k] = 255;
                        else
                        {
                            for (int a = 2; a < count + 1; ++a)
                                if (image.ImageMatrix[i][j, k] >= intervals[a - 1] && image.ImageMatrix[i][j, k] < intervals[a])
                                {
                                    resultImage.ImageMatrix[i][j, k] = (byte)intervals[a - 1];
                                    break;
                                }
                        }
                    }

            return resultImage;
        }

        //Zadanie 7
        /*
         * Funkcja wykonująca erozję obrazu A przez B
         */
        public static MatrixImage Erosion(MatrixImage imageA, MatrixImage imageB, bool isBinary = false)
        {
            MatrixImage tempImage = new MatrixImage(imageA);
            return Negation(Dilation(Negation(tempImage), imageB, isBinary));
        }

        /*
         * Funkcja wykonująca nakładanie obrazu B na A
         */
        public static MatrixImage Dilation(MatrixImage imageA, MatrixImage imageB, bool isBinary = false)
        {
            MatrixImage fooA = isBinary ? ToBinary(imageA) : imageA;

            imageB = ToBinary(imageB);

            MatrixImage resultImage = new MatrixImage(fooA);

            int shiftX = imageB.Width / 2;
            int shiftY = imageB.Height / 2;

            for (int j = 0; j < fooA.Height; ++j)
                for (int k = 0; k < fooA.Width; ++k)
                    if (imageA.ImageMatrix[0][j, k] != 0)
                    {
                        byte max = 0;

                        int startY = j - shiftY >= 0 ? j - shiftY : 0;
                        int startX = k - shiftX >= 0 ? k - shiftX : 0;
                        int endY = j + shiftY < fooA.Height ? j + shiftY : fooA.Height;
                        int endX = k + shiftX < fooA.Width ? k + shiftX : fooA.Width;

                        int startBY = startY == 0 ? shiftY - j : 0;
                        int startBX = startX == 0 ? shiftX - k : 0;
                        int endBY = endY == fooA.Height ? fooA.Height - (fooA.Height - j) : fooA.Height;
                        int endBX = endX == fooA.Width ? fooA.Width - (fooA.Width - k) : fooA.Width;

                        for (int a = startY, aB = startBY; a < endY; ++a, ++aB)
                            for (int b = startX, bB = startBX; b < endX; ++b, ++bB)
                                if (imageB.ImageMatrix[0][aB, bB] != 0)
                                    if (max < fooA.ImageMatrix[0][a, b])
                                        max = fooA.ImageMatrix[0][a, b];

                        for (int a = startY, aB = startBY; a < endY; ++a, ++aB)
                            for (int b = startX, bB = startBX; b < endX; ++b, ++bB)
                                if (imageB.ImageMatrix[0][aB, bB] != 0)
                                    resultImage.ImageMatrix[0][a, b] = max;
                    }

            return resultImage;
        }



        /*
         * Funkcja wykonująca otwarcie obrazu A obrazem B
         */
        public static MatrixImage Opening(MatrixImage imageA, MatrixImage imageB, bool isBinary = false)
        {
            MatrixImage tempImage = new MatrixImage(imageA);
            return Dilation(Erosion(tempImage, imageB, isBinary), imageB, isBinary);
        }

        /*
        * Funkcja wykonująca zamknięcie obrazu A obrazem B
        */
        public static MatrixImage Closing(MatrixImage imageA, MatrixImage imageB, bool isBinary = false)
        {
            MatrixImage tempImage = new MatrixImage(imageA);
            return Erosion(Dilation(tempImage, imageB, isBinary), imageB, isBinary);
        }

        ////////////// ZAD 9
        /*
        * Funkcja wykonująca dowolną filtrację
        */
        public static MatrixImage Filter(MatrixImage image, int[,] matrix)
        {
            MatrixImage resultImage = new MatrixImage(image);

            int matrixSum = 0;

            for (int i = 0; i < matrix.GetLength(0); ++i)
                for (int j = 0; j < matrix.GetLength(1); ++j)
                    matrixSum += Math.Abs(matrix[i, j]);

            int shift = (matrix.GetLength(0) - 1) / 2;
            byte[,] lightnessMatrix = image.GetLightnessMatrix();

            for (int j = shift; j < image.Height - shift; ++j)
                for (int k = shift; k < image.Width - shift; ++k)
                {
                    int sum = 0;
                    byte max = image.Channels == 3 ?
                        (Math.Max(Math.Max(image.ImageMatrix[0][j, k], image.ImageMatrix[1][j, k]), image.ImageMatrix[2][j, k]))
                    : image.ImageMatrix[0][j, k];
                    double scale = max != 0 ? 255.0 / max : 0;

                    for (int a = j - shift, y = 0; a < j + shift + 1; ++a, ++y)
                        for (int b = k - shift, x = 0; b < k + shift + 1; ++b, ++x)
                            sum += lightnessMatrix[a, b] * matrix[x, y];

                    sum /= matrixSum;

                    for (int a = 0; a < resultImage.Channels; ++a)
                        resultImage.ImageMatrix[a][j, k] = (byte)(sum * scale / 255.0 * image.ImageMatrix[a][j, k]);
                }

            return resultImage;
        }

       /*
       * Funkcja wykonująca filtrację dolnoprzepustową
       */
        public static MatrixImage LowPassFilter(MatrixImage image) =>
            Filter(image, new int[,]
            {
                { 1, 1, 1 },
                { 1, 1, 1 },
                { 1, 1, 1 }
            });

        public static MatrixImage LowPassFilterPyramidal(MatrixImage image) =>
            Filter(image, new int[,]
            {
                {1, 2, 3, 2, 1 },
                {2, 4, 6, 4, 2 },
                {3, 6, 9, 6, 3 },
                {2, 4, 6, 4, 2 },
                {1, 2, 3, 2, 1 }
            });

        public static MatrixImage LowPassFilterGauss(MatrixImage image) =>
            Filter(image, new int[,]
            {
                {1, 1, 1, 1, 1 },
                {1, 4, 6, 4, 1 },
                {1, 1, 1, 1, 1 },
                {1, 4, 6, 4, 1 },
                {1, 1, 1, 1, 1 }
            });

        public static MatrixImage RobertsFilter(MatrixImage image) =>
            Filter(image, new int[,]
            {
                { 0, 0, 0 },
                { 0, 1,-1 },
                { 0, 0,-0 }
            });

        public static MatrixImage PrewittFilter(MatrixImage image) =>
            Filter(image, new int[,]
            {
                { 1, 1, 1 },
                { 0, 0, 0 },
                {-1,-1,-1 }
            });

        public static MatrixImage SobelFilter(MatrixImage image) =>
            Filter(image, new int[,]
            {
                { 1, 0,-1 },
                { 2, 0,-2 },
                { 1, 0,-1 }
            });

        public static MatrixImage KirschFilter(MatrixImage image) =>
            Filter(image, new int[,]
            {
                {-5, 3, 3 },
                {-5, 3, 3 },
                {-5, 3, 3 }
            });
        
        public static MatrixImage CompassPrewittilter(MatrixImage image) =>
            Filter(image, new int[,]
            {
                { 1, 1, 1 },
                {-1,-2, 1 },
                {-1,-1, 1 }
            });
    
        /*
        * Funkcja wykonująca filtracje na podstawie indeksu piksela w oknie 3x3 po sortowaniu
        */
        public static MatrixImage ValueFilter(MatrixImage image, int index)
        {
            MatrixImage resultImage = new MatrixImage(image);

            byte[,] lightnessMatrix = image.GetLightnessMatrix();

            for (int j = 1; j < image.Height - 1; ++j)
                for (int k = 1; k < image.Width - 1; ++k)
                {
                    byte max = image.Channels == 3 ?
                        (Math.Max(Math.Max(image.ImageMatrix[0][j, k], image.ImageMatrix[1][j, k]), image.ImageMatrix[2][j, k]))
                    : image.ImageMatrix[0][j, k];
                    double scale = max != 0 ? 255.0 / max : 0;

                    byte[] pixelValues = new byte[9];

                    for (int a = j - 1, x = 0; a < j + 2; ++a)
                        for (int b = k - 1; b < k + 2; ++b, ++x)
                            pixelValues[x] = lightnessMatrix[a, b];

                    Array.Sort(pixelValues);
                    byte pixelValue = pixelValues[index];

                    for (int a = 0; a < resultImage.Channels; ++a)
                        resultImage.ImageMatrix[a][j, k] = (byte)(pixelValue * scale / 255.0 * image.ImageMatrix[a][j, k]);
                }

            return resultImage;
        }


        public static MatrixImage MedianFilter(MatrixImage image) =>  ValueFilter(image, 4);
        public static MatrixImage MinFilter(MatrixImage image) =>  ValueFilter(image, 0);
        public static MatrixImage MaxFilter(MatrixImage image) =>  ValueFilter(image, 8);
    }
}
