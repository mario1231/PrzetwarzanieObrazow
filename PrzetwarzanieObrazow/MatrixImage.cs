﻿using System;
using System.IO;
using System.Linq;
using System.Windows.Controls;
using System.Windows.Forms;
using System.Windows.Media;
using System.Windows.Media.Imaging;

/*
 *  Klasa reprezentująca obraz. 
 *  Zawiera metody pozwalające na wczytanie obrazu, wyświetlenie go w obiekcie 
 *  canvas oraz na edycję macierzy obrazu.
 */
namespace PrzetwarzanieObrazow
{
    public class MatrixImage
    {
        //Strumień używany do wczytania obrazu z pliku
        private Stream imageStream = null;
        //Dekoder JPEG używany do dekodowania wczytanego obrazu
        private JpegBitmapDecoder decoder = null;
        //Obiekt będący reprezentacją bitmapową zdekodowanego obrazu
        private BitmapSource bitmapSource = null;

        //Szerokość obrazu w pikselach
        private int width = 0;
        //Wysokość obrazu w pikselach
        private int height = 0;
        //Licba kanałów obrazu: 1 - dla obrazów szarych, 3 - dla obrazów RGB
        private int channels = 0;
        //Krok bitmapy opisany wzorem: width * ((bitsPerPixel + 7) / 8)
        private int stride = 0;

        /*
         * Macierz reprezentująca zbiór pikseli obrazu.
         * Jej wymiary są następujące: [liczba_kanałów][szerokość, wysokość]
         * 
         * Więc jest to macierz 2 wymiarowa, z tym że drugi wymiar ma 2 kierunki.
         * Np. żeby pobrać piksel o indeksach (30, 40) z kanału 2 zapiszemy: [2][30, 40]
         */
        private byte[][,] imageMatrix = null;
        private byte[][,] imageMatrixNotNormalized = null;

        private double[][] histogram = null;

        /*
         * Domyślny konstruktor, nie robi nic.
         */
        public MatrixImage()
        {

        }

        /*
         * Konstruktor kopiujący - kopiuje macierz z podanego obrazu oraz wszystkie inne parametry 
         * opisujące obraz image.
         * 
         * @params: MatrixImage image - wczytany obraz.
         */
        public MatrixImage(MatrixImage image)
        {
            width = image.Width;
            height = image.Height;
            channels = image.Channels;
            stride = image.Stride;

            histogram = new double[image.Channels][];
            for (int i = 0; i < image.Channels; ++i)
                histogram[i] = new double[256];

            for (int i = 0; i < image.Channels; ++i)
                for (int j = 0; j < 256; ++j)
                    histogram[i][j] = image.Histogram[i][j];

            imageMatrix = new byte[channels][,];
            for (int i = 0; i < channels; ++i)
                imageMatrix[i] = new byte[height, width];

            imageMatrixNotNormalized = new byte[channels][,];
            for (int i = 0; i < channels; ++i)
                imageMatrixNotNormalized[i] = new byte[height, width];

            for (int i = 0; i < channels; ++i)
                for (int j = 0; j < height; ++j)
                    for (int k = 0; k < width; ++k)
                        imageMatrix[i][j, k] = image.ImageMatrix[i][j, k];

            bitmapSource = BitmapSource.Create(
                width,
                height,
                96,
                96,
                image.BitmapSource.Format,
                image.BitmapSource.Palette,
                MatrixToVector(imageMatrix, image.bitmapSource.Format),
                stride
            );
        }

        /*
         * Funkcja wczytująca plik obraz konwertująca go na wymaganą przez nas postać.
         * 
         * @params: String filename - nazwa pliku
         */
        public void FromFile(String filemane)
        {
            //Create vector of pixels
            byte[] imageVector;

            //Read and decode JPEG image
            imageStream = new FileStream(filemane, FileMode.Open, FileAccess.Read, FileShare.Read);
            decoder = new JpegBitmapDecoder(imageStream, BitmapCreateOptions.PreservePixelFormat, BitmapCacheOption.Default);
            bitmapSource = decoder.Frames[0];

            width = bitmapSource.PixelWidth;
            height = bitmapSource.PixelHeight;
            channels = bitmapSource.Format.BitsPerPixel / 8;
            stride = bitmapSource.PixelWidth * ((bitmapSource.Format.BitsPerPixel + 7) / 8);

            imageVector = new byte[bitmapSource.PixelHeight * bitmapSource.PixelWidth * channels];

            bitmapSource.CopyPixels(imageVector, stride, 0);
            imageMatrix = VectorToMatrix(imageVector, bitmapSource.PixelWidth, bitmapSource.PixelHeight, channels, bitmapSource.Format);
            imageMatrixNotNormalized = VectorToMatrix(imageVector, bitmapSource.PixelWidth, bitmapSource.PixelHeight, channels, bitmapSource.Format);

            bitmapSource = BitmapSource.Create(
                width,
                height,
                96,
                96,
                bitmapSource.Format,
                bitmapSource.Palette,
                imageVector,
                stride
            );

            //Calculate histogram
            histogram = ImageOperations.CalculateHistogram(this);
        }

        /*
         * Funkcja sprawdza czy obraz został wczytany i zaninicjalizowany poprawnie i można go użyć
         */

        public bool IsOK()
        {
            return this.bitmapSource != null;
        }

        /*
         * Funkcja zmienia rozmiary macierzy obrazu, nowe piksele wypełniając czernią.
         */
        public void ResizeMatrix(int newWidth, int newHeight)
        {
            byte[][,] newMatrix = new byte[channels][,];

            for (int i = 0; i < channels; ++i) {
                newMatrix[i] = new byte[newHeight, newWidth];

                for (int j = 0; j < newHeight; ++j)
                    for (int k = 0; k < newWidth; ++k)
                        newMatrix[i][j, k] = 0;

                for (int j = 0; j < Math.Min(height, newHeight); ++j)
                    for (int k = 0; k < Math.Min(width, newWidth); ++k)
                        newMatrix[i][j, k] = imageMatrix[i][j, k];
            }

            Update(newMatrix);
        }
        
        /*
         * Funkcja kopiuje macierz wejściową w miejsce macierzy obrazu
         * i aktualizuje wszystkie parametry obrazu.
         */
        public void Update(byte[][,] matrix)
        {
            imageMatrix = matrix;
            imageMatrixNotNormalized = matrix;

            width = matrix[0].GetLength(1);
            height = matrix[0].GetLength(0);
            channels = matrix.Length;
            stride = width * ((bitmapSource.Format.BitsPerPixel + 7) / 8);

            bitmapSource = BitmapSource.Create(
                width,
                height,
                96,
                96,
                bitmapSource.Format,
                bitmapSource.Palette,
                MatrixToVector(matrix, bitmapSource.Format),
                stride
            );

            //Calculate histogram
            histogram = ImageOperations.CalculateHistogram(this);
        }

        public void Clean()
        {
            for (int i = 0; i < channels; ++i)
            {
                for (int j = 0; j < height; ++j)
                    for (int k = 0; k < width; ++k)
                        imageMatrix[i][j, k] = 0;
            }

            Update(imageMatrix);
        }

        /*
         * Funkcja rysująca obraz w obiekcie canvas.
         * 
         * @params: Canvas canvas - płótno w którym chcemy rysować.
         */
        public void DrawInCanvas(Canvas canvas, bool normalized = true)
        {
            bitmapSource = BitmapSource.Create(
                bitmapSource.PixelWidth,
                bitmapSource.PixelHeight,
                bitmapSource.DpiX,
                bitmapSource.DpiY,
                bitmapSource.Format,
                bitmapSource.Palette,
                MatrixToVector(normalized ? imageMatrix : imageMatrixNotNormalized, bitmapSource.Format),
                bitmapSource.PixelWidth * ((bitmapSource.Format.BitsPerPixel + 7) / 8)
            );

            //Draw rectangle in canvas
            ImageBrush imageBrush = new ImageBrush();
            imageBrush.ImageSource = bitmapSource;
            System.Windows.Shapes.Rectangle rectangle = new System.Windows.Shapes.Rectangle();

            rectangle.Width = width;
            rectangle.Height = height;
            rectangle.Fill = imageBrush;

            Canvas.SetLeft(rectangle, (canvas.Width - rectangle.Width) * 0.5);
            Canvas.SetTop(rectangle, (canvas.Height - rectangle.Height) * 0.5);

            canvas.Children.Clear();
            canvas.Children.Add(rectangle);
        }

        /*
         * Funkcja wyświetlająca informacje o obrazie w GUI.
         * @params: 
         *      TextBlock width, height, color, dpi - odpowiednie textblocki
         *      Canvas r, g, b - canvasy do narysowania histogramów
         */
        public void ShowInfo(TextBlock widthTB, TextBlock heightTB, TextBlock colorTB, TextBlock dpiTB, Canvas r, Canvas g, Canvas b)
        {
            widthTB.Text = width.ToString();
            heightTB.Text = height.ToString();
            colorTB.Text = channels == 3 ? "RGB" : "SKALA SZAROŚCI";
            dpiTB.Text = bitmapSource.DpiX.ToString();

            DrawHistogramInCanvas(r, g, b);
        }

        public void DrawHistogramInCanvas(Canvas r, Canvas g, Canvas b)
        {
            double width = r.ActualWidth;
            double height = r.ActualHeight;
            double step = (width - 30) / 256.0;

            Canvas[] canvases = { r, g, b };
            SolidColorBrush[] colors = {
                new SolidColorBrush(Colors.Red),
                new SolidColorBrush(Colors.Green),
                new SolidColorBrush(Colors.Blue)
            };

            r.Children.Clear();
            g.Children.Clear();
            b.Children.Clear();

            for (int i = 0; i < this.channels; ++i)
            {
                double max = histogram[i].Max();

                TextBlock text = new TextBlock();
                text.Text = Convert.ToString(max) + "%";
                text.FontSize = 10;

                Canvas.SetLeft(text, 30);
                Canvas.SetTop(text, 5);

                canvases[i].Children.Add(text);

                System.Windows.Shapes.Line lineX = new System.Windows.Shapes.Line();
                lineX.StrokeThickness = 1;
                lineX.Stroke = new SolidColorBrush(Colors.Black);
                lineX.X1 = 20;
                lineX.X2 = canvases[i].ActualWidth - 10;
                lineX.Y1 = lineX.Y2 = 190;

                System.Windows.Shapes.Line lineY = new System.Windows.Shapes.Line();
                lineY.StrokeThickness = 1;
                lineY.Stroke = new SolidColorBrush(Colors.Black);
                lineY.X1 = lineY.X2 = 20;
                lineY.Y1 = 10;
                lineY.Y2 = canvases[i].ActualHeight - 10;

                canvases[i].Children.Add(lineX);
                canvases[i].Children.Add(lineY);

                System.Windows.Shapes.Polygon line = new System.Windows.Shapes.Polygon();
                line.StrokeThickness = 1;
                line.Fill = colors[i];

                PointCollection points = new PointCollection();

                for (int j = 0; j < 256; ++j)
                    points.Add(
                        new System.Windows.Point(
                            j * step + 20,
                            (height - 10) - (histogram[i][j] / max * (height - 20))
                        )
                    );

                points.Add(
                    new System.Windows.Point(
                        width - 10,
                        height - 10
                    )
                );

                points.Add(
                    new System.Windows.Point(
                        20,
                        height - 10
                    )
                );

                line.Points = points;
                
                canvases[i].Children.Add(line);
            }
        }

        /*
         * Funkcja konwertująca macierz obrazu na wektor pikseli.
         * 
         * @params: byte[][,] matrix - macierz obrazu.
         * @return: byte[] - wektor pikseli
         */
        public byte[] MatrixToVector(byte[][,] matrix, PixelFormat pixelFormat)
        {
            byte[] vector = new byte[matrix.Length * width * height];
            int[] channelOrder = { 0, 1, 2 };

            if (pixelFormat == PixelFormats.Bgr24)
                Array.Reverse(channelOrder);

            for (int i = 0, m = 0; i < height; ++i)
                for (int j = 0; j < width; ++j)
                    for (int k = 0; k < channels; ++k)
                        vector[m++] = matrix[channelOrder[k]][i, j];

            return vector;
        }

        /*
         * Funkcja konwertuje wektor pikseli na macierz pikseli.
         * 
         * @params: byte[] vector - wektor pikseli
         * @return: byte[][,] - macierz obrazu.
        */
        public byte[][,] VectorToMatrix(byte[] vector, int width, int height, int channels, PixelFormat pixelFormat)
        {
            int stride = width * channels;
            int size = stride * height;
            int[] channelOrder = { 0, 1, 2 };

            if (pixelFormat == PixelFormats.Bgr24)
                Array.Reverse(channelOrder);

            byte[][,] matrix = new byte[channels][,];
            for (int i = 0; i < channels; ++i)
                matrix[i] = new byte[height, width];

            for (int i = 0, m = 0; i < height; ++i)
                for (int j = 0; j < width; ++j)
                    for (int k = 0; k < channels; ++k)
                        matrix[channelOrder[k]][i, j] = vector[m++];

            return matrix;
        }

        public byte[,] GetLightnessMatrix()
        {
            byte[,] hslMatrix = new byte[height, width];

            if (channels == 1)
                for (int i = 0; i < height; ++i)
                    for (int j = 0; j < width; ++j)
                        hslMatrix[i, j] = imageMatrix[0][i, j];
            else
                for (int i = 0; i < height; ++i)
                    for (int j = 0; j < width; ++j)
                        hslMatrix[i, j] = Math.Max(Math.Max(imageMatrix[0][i, j], imageMatrix[1][i, j]), imageMatrix[2][i, j]);

            return hslMatrix;
        }

        //Gettery i settery
        public int Width { get { return width; } }
        public int Height { get { return height; } }
        public bool IsGrayScale { get { return channels == 1; } }
        public int Channels { get { return channels; } }
        public int Stride { get { return stride; } }
        public double[][] Histogram { get { return histogram; } set { histogram = value; } }
        public byte[][,] ImageMatrix { get { return imageMatrix; }  set { imageMatrix = value; } }
        public byte[][,] ImageMatrixNotNormalized { get { return imageMatrixNotNormalized; }  set { imageMatrix = value; } }
        public BitmapSource BitmapSource { get { return bitmapSource; } set { bitmapSource = value; } }
    }
}
